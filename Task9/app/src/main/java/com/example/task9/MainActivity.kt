package com.example.task9

import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private var img:ImageView? = null
    private var button:Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        img = findViewById(R.id.image)
        button = findViewById<Button>(R.id.button)
        button?.setOnClickListener(this::changeImg)
    }

    fun changeImg(view: View){
        var x:Int = retInt()
        button?.setBackgroundColor(Color.rgb(Random.nextInt(0, 255), Random.nextInt(0, 255), Random.nextInt(0, 255)))
        when(x){
            1->img?.setImageResource(R.drawable.dragon1)
            2->img?.setImageResource(R.drawable.dragon2)
            3->img?.setImageResource(R.drawable.dragon3)
            4->img?.setImageResource(R.drawable.drgaon4)
            5->img?.setImageResource(R.drawable.dragon5)
            6->img?.setImageResource(R.drawable.dragon6)
            7->img?.setImageResource(R.drawable.dragon7)
            8->img?.setImageResource(R.drawable.dragon8)
            9->img?.setImageResource(R.drawable.dragon9)

        }
    }

    fun retInt():Int{
        val random:Int = Random.nextInt(1, 9)
        return random
    }
}